<?php
/**
 * 用户数据操作类
 * 相当于一个数据库的操作类，完成增删改查的操作
 */
class WxUser
{
    private $full_path;

    // 系统生成的一个随机字符串，用着二维码的 sence 标识
    const WECHAT_FLAG = 'from_wechat';

    // 生成的临时二维码url
    const QR_URL = 'qr_url_';

    // 登录态的存储 微信事件告知我的openid
    const LOGIN_WECHAT = 'login_wechat';

    private $CI;

    public function __construct()
    {

        $this->CI =& get_instance();
        $this->CI->load->helper('file');

        $this->full_path = APPPATH . 'data/user.txt';
    }

    public function getOne($openid)
    {
        $users = $this->get();
		return $users[$openid] ?? [];
	}

    public function setOne($user, $openid):bool
    {
        if (!$user) {
            return false;
        }

        if (!$openid) {
            return false;
        }

        $users = $this->get();
        $users[$openid] = $user;

        return $this->set($users);
    }

	private function set(array $users):bool
    {
		if (!$users) {
			return false;
		}

        //log_message('debug', file_exists($this->full_path));
        //log_message('debug', APPPATH . 'data');
        if (!file_exists($this->full_path)) {
			mkdir(APPPATH . 'data', DIR_WRITE_MODE, TRUE);
		}


		return write_file($this->full_path, serialize($users));
	}

    private function get():array
    {
		$unserializ_data = file_get_contents($this->full_path);
		if (!$unserializ_data) {
			return [];
		}

		$serializ_data = unserialize($unserializ_data);

		return $serializ_data;
	}
}
