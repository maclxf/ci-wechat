<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use EasyWeChat\Foundation\Application;
class EWechat {
    private $app;

    private $CI;

	public function __construct()
    {
        $this->CI =& get_instance();


        $this->CI->load->helper('file');
        $this->CI->load->library('WxUser');
        $this->CI->load->driver('cache');
        $this->CI->load->helper('string');

        $options = C('wechat', 'easy');
        $this->app = new Application($options);
    }

    public function serve()
    {
        //write_file('/home/www/wechat.php', $message);
        $this->app->server->setMessageHandler(function ($message) {
            //write_file('./logs/wechat.php', $message);
            // switch ($message->MsgType) {
            //     case 'event':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到事件消息';
            //         break;
            //     case 'text':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到文字消息';
            //         break;
            //     case 'image':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到图片消息';
            //         break;
            //     case 'voice':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到语音消息';
            //         break;
            //     case 'video':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到视频消息';
            //         break;
            //     case 'location':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到坐标消息';
            //         break;
            //     case 'link':
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到链接消息';
            //         break;
            //     // ... 其它消息
            //     default:
            //         return $message->MsgType . ' ' .  $message->ToUserName . ' ' . $message->FromUserName . ' ' . $message->CreateTime . ' ' . '收到其它消息';
            //         break;
            // }
            // ...
            if ($message) {
                $method = camelize('handle_' . $message->MsgType);
                log_message('debug', $method);
                if (method_exists($this, $method)) {
                    $this->openid = $message['FromUserName'];

                    return call_user_func_array([$this, $method], [$message]);
                }
            }
            log_message('debug', __FUNCTION__ . 'NOT SUCCESS');
        });

        $response = $this->app->server->serve();

        $response->send();
    }

    public function getTempCode()
    {

        // scence code
        // 避免重复生成
        $wechat_flag = get_cookie(WxUser::WECHAT_FLAG);
        if (!$wechat_flag) {
            $wechat_flag = random_string('md5');
        }

        //$a = $this->CI->cache->file->delete('scanId');
        //var_dump($a);
        // 检查一下有缓存的二维码url没得
        $url = $this->CI->cache->file->get(WxUser::QR_URL . $wechat_flag);

        if (!$url) {
            //log_message('debug', '没得');
            // 兑换创建二维码需要的ticket
            $result = $this->app->qrcode->temporary($wechat_flag, 60*60*24);
            $ticket = $result->ticket;// 或者 $result['ticket']
            // $expireSeconds = $result->expire_seconds; // 有效秒数
            // $url = $result->url; // 二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片

            $url = $this->app->qrcode->url($ticket);// 二维码图片本身的地址

            // 把这个二维码的地址本身拿来存起，方便后续使用避免重复调用微信接口
            $this->CI->cache->file->save(WxUser::QR_URL . $wechat_flag, $url, 60*60*24);
        }

        // scence code 存下来
        set_cookie(WxUser::WECHAT_FLAG, $wechat_flag, 3600*24);

        return [
            'url' => $url,
            'wechat_flag' => $wechat_flag
        ];
    }

    public function checkLogin($flag)
    {
        if (!$flag) {
            return [
                'status' => false,
                'img' => '',
                'msg' => '少参数'
            ];
        }

        $openid = $this->CI->cache->file->get(WxUser::LOGIN_WECHAT . $flag);

        $user = $this->CI->wxuser->getOne($openid);
        if (!$user) {
            return [
                'status' => false,
                'img' => '',
                'msg' => '没找到人'
            ];
        }

        set_session(
            'user',
            json_encode($user)
        );

        // 删除登录态
        $this->CI->cache->file->delete(WxUser::LOGIN_WECHAT . $flag);

        // 有人登录了这个二维码url就废了不用了
        $this->CI->cache->file->delete(WxUser::QR_URL . $flag);

        return [
            'status' => true,
            'img' => $user->headimgurl,
            'user' => $user,
            'msg' => ''
        ];
    }

    protected function handleEvent($event)
    {
        if ($event) {
            $method = camelize('event_' . $event->Event);
            log_message('debug', $method);
            if (method_exists($this, $method)) {
                return call_user_func_array([$this, $method], [$event]);
            }
        }
        log_message('debug', __FUNCTION__ . 'NOT SUCCESS');
    }

    protected function eventSCAN($event)
    {
        //log_message('debug', $this->app->user->get($this->openid));
        //eventSCAN
        //{"ToUserName":"gh_e4fde6aa0de7","FromUserName":"onQMzwP34Dj5iNxdDjOlaKJt6Omc","CreateTime":"1614222382","MsgType":"event","Event":"SCAN","EventKey":"e1ce7fdf8ae3b419e79963d07a40b832","Ticket":"gQEq8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyazlIZTlZdDNjUDAxNkZ6VU53Y0oAAgQpEjdgAwSAUQEA"}

        $user = $this->CI->wxuser->getOne($this->openid);
        if ($user) {
            $this->doLogin($event, $this->openid);
        } else {
            $wechatUser = $this->app->user->get($this->openid);

            $this->CI->wxuser->setOne($wechatUser, $this->openid);

            $this->doLogin($event, $this->openid);
        }
    }

    protected function eventSubscribe($event)
    {
        //eventSubscribe
        //{"ToUserName":"gh_e4fde6aa0de7","FromUserName":"onQMzwP34Dj5iNxdDjOlaKJt6Omc","CreateTime":"1614225661","MsgType":"event","Event":"subscribe","EventKey":"qrscene_e1ce7fdf8ae3b419e79963d07a40b832","Ticket":"gQEq8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyazlIZTlZdDNjUDAxNkZ6VU53Y0oAAgQpEjdgAwSAUQEA"}

        $user = $this->CI->wxuser->getOne($this->openid);
        if ($user) {
            $this->doLogin($event, $this->openid);
            return;
        }

        $wechatUser = $this->app->user->get($this->openid);

        $this->CI->wxuser->setOne($wechatUser, $this->openid);

        $this->doLogin($event, $this->openid);
    }

    protected function doLogin($event, $openid)
    {
        // {
        //     "ToUserName":"gh_e4fde6aa0de7",
        //     "FromUserName":"onQMzwP34Dj5iNxdDjOlaKJt6Omc",
        //     "CreateTime":"1614222382",
        //     "MsgType":"event",
        //     "Event":"SCAN",
        //     "EventKey":"e1ce7fdf8ae3b419e79963d07a40b832",
        //     "Ticket":"gQEq8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyazlIZTlZdDNjUDAxNkZ6VU53Y0oAAgQpEjdgAwSAUQEA"
        // }

        // {
        //     "ToUserName":"gh_e4fde6aa0de7",
        //     "FromUserName":"onQMzwP34Dj5iNxdDjOlaKJt6Omc",
        //     "CreateTime":"1614225661",
        //     "MsgType":"event",
        //     "Event":"subscribe",
        //     "EventKey":"qrscene_e1ce7fdf8ae3b419e79963d07a40b832",
        //     "Ticket":"gQEq8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyazlIZTlZdDNjUDAxNkZ6VU53Y0oAAgQpEjdgAwSAUQEA"
        // }
        if (empty($event->EventKey)) {
            return;
        }

        $eventKey = $event->EventKey;

        if ($event['Event'] === 'subscribe') {
            $eventKey = str_after($event->EventKey, 'qrscene_');
        }

        // 设置登录态
        $this->CI->cache->file->save(WxUser::LOGIN_WECHAT . $eventKey, $openid, 60 * 30);
    }

    private function showArray($params)
    {
        log_message('debug', $params);
    }

























    protected function eventUnsubscribe($event)
    {
        return '用户：' . $this->openid . ' ' . '取关注';
    }
}
