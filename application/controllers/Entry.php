<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entry extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library([
            'EWechat'
        ]);
    }

	public function serve()
	{
        $this->ewechat->serve();
	}

    public function getTempCode()
    {
        $ret = $this->ewechat->getTempCode();

        output($ret);
    }

    public function checkLogin() {
        $wechatFlag = $this->input->post('wechat_flag');
        $ret = $this->ewechat->checkLogin($wechatFlag);
        output($ret);
    }

    public function getUser() {
        $user = get_current_user_info();
        if ($user) {
            output([
                'status' => true,
                'img' => $user->headimgurl,
                'user' => $user,
                'msg' => ''
            ]);
        } else {
            output([
                'status' => false,
                'img' => '',
                'msg' => '请登录'
            ]);
        }
    }

    public function logout() {
        unset_session('user');
        output([
            'status' => true,
            'img' => '',
            'msg' => '退出成功'
        ]);
    }


}
