<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invite extends CI_Controller {

    public function __construct()
    {
        parent::__construct();


    }

    public function location()
    {
        output([
            'date' => '2022年1月2日 中午12点（星期日）',
            'hotel' => '俏巴渝',
            'lunar' => '冬月三十',
            'address' => '重庆市渝中区龙湖时代天街D馆L4',
            'content' => '黎桐语小朋友一周岁生日宴',
            'lat' => '29.533909',
            'lon' => '106.514002',
            'name' => '俏巴渝',
            'shareTitle' => '邀请你来参加，黎桐语小朋友的一周岁生日宴',
            'father' => '黎晓峰',
            'mother' => '梅元军',
            'contract' => [
              [
                  'name' => 'father',
                'tel' => '13883190402',
                'content' => '联系我爸爸',
              ],
              [
                  'name' => 'mother',
                'tel' => '15086985132',
                'content' => '联系我妈妈',
              ],
              [
                  'name' => 'grandmother',
                'tel' => '15213243661',
                'content' => '联系我奶奶',
              ],
              [
                  'name' => 'grandma',
                'tel' => '1398373626',
                'content' => '联系我外婆',
              ],
            ]
        ]);
    }

    public function main()
    {
        output([
            'shareTitle' => '邀请你来参加，黎桐语小朋友的一周岁生日宴',
            'thumb' => base_url('static/images/thumb.jpg?v=8'),
            'timeLineThumb' => base_url('static/images/inv.png'),
            'music' => [
                'title' => 'Battles and Wastelands',
                'url' => base_url('static/audio-1.m4s')
            ],
            'cover' => [
                ['image' => base_url('static/images/cover.jpg?v=8')],
                ['image' => base_url('static/images/cover-1.jpg?v=8')],
                ['image' => base_url('static/images/cover-2.jpg?v=8')],
                ['image' => base_url('static/images/cover-3.jpg?v=8')],
                ['image' => base_url('static/images/cover-4.jpg?v=8')],
                ['image' => base_url('static/images/cover-5.jpg?v=8')],
                ['image' => base_url('static/images/cover-6.jpg?v=8')],
                ['image' => base_url('static/images/cover-7.jpg?v=8')]
            ]
        ]);
    }


}
