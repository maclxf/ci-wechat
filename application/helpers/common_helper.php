<?php


function output($data, $encoded = TRUE) {
	$CI =& get_instance();

	if ($encoded) {
		$CI->output->set_content_type('application/json');
		$data = json_encode($data);
	}

	$CI->output->set_output($data);
}

function str_after($haystack, $mixedneedle)
{
    $newString = strstr($haystack, $mixedneedle);
    $length = strlen($mixedneedle);
    return substr($newString, $length);
}

/**
 * 获取指定名称的session
 * @param  string $name session名称
 * @return string       session的值
 */
function get_session($name) {
	$CI =& get_instance();
	return $CI->session->userdata($name);
}

/**
 * 把指定信息添加到session中
 *
 * @param string | array  $name   要设置的数据或名称
 * @param mixed           $value  要设置的值
 */
function set_session($name, $value = NULL) {
	$CI =& get_instance();

	if ($name) {
		if (is_string($name)) {
			if (is_array($value)) {
				$value = serialize($value);
			}
			$CI->session->set_userdata($name, $value);
		} else if (is_array($name)) {
			$CI->session->set_userdata($name);
		}
	}
}

/**
 * 从session中删除值
 *
 * @param  string | array $name 要删除的值的name(或由name组成的数组)
 */
function unset_session($name) {
	$CI =& get_instance();
	$CI->session->unset_userdata($name);
}

function get_current_user_info() {
    $user = get_session('user');

    return json_decode($user);
}
