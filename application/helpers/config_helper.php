<?php
/************************************************************
 * 获取配置通用方法
 ************************************************************/

/**
 * 获取指定配置文件下指定的配置项
 * @param  string $file 配置文件名
 * @param  string $key  数组的key
 * @return mix          配置的值
 */
function C($file, $key) {
	$value = '';

	global $CI;

	if (!$CI) {
		$CI =& get_instance();
	}

	// 第3个参数TRUE表明当要载入的文件不存在时，阻止页面报错
	$loaded = $CI->load->config($file, FALSE, TRUE);
	/*if ($loaded) {*/
		$value = $CI->config->item($key);
	/*}*/

	return $value;
}

/* End of file common.php */
/* Location: ./shared/helpers/common.php */